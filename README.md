# Arial Unicode MS 字体文件

## 描述
本仓库提供了一个名为 `arial unicode ms.rar` 的资源文件，该文件包含了 SolidWorks 导出 PDF 时所需的 "Arial Unicode MS" 字体文件。当 SolidWorks 提示字体 "Arial Unicode MS" 安装不正确时，可以使用本资源文件中的字体文件进行修复。

## 使用方法
1. 下载 `arial unicode ms.rar` 文件。
2. 解压缩 `arial unicode ms.rar` 文件，获取其中的字体文件。
3. 将解压后的字体文件复制到 `C:\Windows\Fonts` 目录下。
4. 重新启动 SolidWorks，并尝试导出 PDF 文件，问题应该得到解决。

## 注意事项
- 请确保在复制字体文件到 `C:\Windows\Fonts` 目录时，系统管理员权限已获得。
- 如果字体文件已经存在于 `C:\Windows\Fonts` 目录下，请先备份原有文件，再进行替换。

## 贡献
如果您在使用过程中遇到任何问题或有改进建议，欢迎提交 Issue 或 Pull Request。

## 许可证
本资源文件遵循开源许可证，具体信息请参考 LICENSE 文件。